import React from 'react'
// import { makeStyles } from '@material-ui/core/styles'
import useStyles from './ProfileCSS'
import profilePhoto from '../../Assets/user.png'
import TextField from '@material-ui/core/TextField'
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import EditIcon from '@material-ui/icons/Edit';

function Profile() {
    const [editable, setEditable] = React.useState(true)
    const classes = useStyles();
    const matches = useMediaQuery('(min-width:1184px)');

    const enableEdit = () => {
        setEditable(false)
    }

    return (
        <div className={classes.container}>
            <div className={classes.heading}>
                <Typography variant="h5">
                    Profile
                </Typography>
            </div>
            <Card className={matches ? classes.horizontalProfile : classes.verticalProfile}>
                <div className={matches ? classes.basicDetailsWithBorder:classes.basicDetails}>
                    
                    <div className={classes.profileImg}>
                            <img src={profilePhoto} alt="img" className={classes.Img}/>
                            <TextField 
                            type="file"
                            InputProps={{disableUnderline: true}}
                            />
                    </div>
                    <TextField 
                            type="text"
                            required
                            name="name"
                            label="Name"
                            value="Student1"
                            className={classes.textField}
                    />
                    <TextField 
                            type="email"
                            required
                            name="email"
                            label="Email"
                            value="student1@papershala.com"
                            className={classes.textField}
                    />
                    <div className={classes.phoneNumber}>
                        <TextField 
                                type="tel"
                                disabled={editable}
                                name="mobile"
                                label="Mobile"
                                value="9123456789"
                                className={classes.textField}
                        />
                        <EditIcon onClick={enableEdit} className={classes.editIcon}/>
                    </div>
                        <Button type="submit" className={classes.btnSave}>Save Changes</Button>
                </div>
                <div className={classes.additionalDetails}>
                    <Typography variant="h6" className={classes.subtitle}>Additional Details</Typography>
                    <TextField 
                        type="text"
                        name="address"
                        label="Address"
                        value="Address"
                        className={classes.textField}
                    />
                    <TextField 
                        type="text"
                        name="city"
                        label="City"
                        value="Bangalore"
                        className={classes.textField}
                    />
                    <TextField 
                        type="text"
                        name="state"
                        label="State"
                        value="Karnataka"
                        className={classes.textField}
                    />
                    <TextField 
                        type="text"
                        name="country"
                        label="Country"
                        value="India"
                        className={classes.textField}
                    />
                </div>
            </Card>
        </div>
    )
}

export default Profile