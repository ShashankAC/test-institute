import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
    container: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'start'
    },
    horizontalProfile: {
        width: "90%",
        display: 'flex',
        margin: '10px',
        justifyContent: 'center',
        backgroundColor: '#EBEBEB'
    },
    verticalProfile: {
        width: "90%",
        display: 'flex',
        flexDirection: 'column',
        margin: '10px',
        justifyContent: 'left',
        alignItems: 'start',
        backgroundColor: '#EBEBEB'
    },
    basicDetailsWithBorder: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'start'
    },
    basicDetails: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'start',
    },
    additionalDetails: {
        width: "40%",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'start',
        minWidth: '455px'
    },
    profileImg: {
        marginBottom: "20px",
        display: 'flex'
    },
    Img: {
        height: '150px',
        width: '150px',
        borderRadius: "75px",
        margin: '5px',
        objectFit: 'contain'
    },
    textField: {
        marginTop: '10px',
        marginLeft: '5px',
        marginBottom: '10px',
        minWidth: '200px'
    },
    heading: {
        width: "90%",
        textAlign: 'left',
        margin:'5px'
    },
    btnSave: {
        backgroundColor: '#3C8DBC',
        marginTop: '25px',
        marginBottom: '10px',
        width: '250px'
    },
    phoneNumber: {
        display: 'flex',
        justifyContent: 'left',
        alignItems: 'center'
    },
    editIcon: {
        cursor: 'pointer'
    },
    subtitle: {
        marginLeft: '5px'
    }
}))

export default useStyles