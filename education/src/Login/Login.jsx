import React,{ useState } from 'react'
import Container from '@material-ui/core/Container'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import TextField from '@material-ui/core/TextField';
import CardHeader from '@material-ui/core/CardHeader';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { updateAuthState } from '../Store/actions/authAction'
import { connect } from "react-redux";
import Button from '@material-ui/core/Button';
import { InputAdornment, IconButton } from "@material-ui/core";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { Link } from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
    container: {
      backgroundColor: "#0B4B7F",
      height: "100%",
      width: "100%",
      display: "flex",
      justifyContent: "center",
      alignItems: "center"
    },
    card: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between'
      },
    inputField: {
        margin: '5px'
      },
    btnLogin: {
        backgroundColor: "#198235",
        '&:hover': {
          background: "#7CCAE3",
       },
        width: "100px",
      },
      btnRegister: {
        backgroundColor: "#FF1737",
        '&:hover': {
          background: "#FF1737",
       },
        width: "100px",
      },
      header: {
        backgroundColor: "",
        color: "black"
      },
      actions: {
        display: 'flex',
        justifyContent: 'space-around'
      },
      forgotPassword: {
        marginTop: "10px",
        marginBottom: "10px",
        textDecoration: "none"
      },
      backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
      }
  }));


function Login(props) {
    const [phoneNumber, setPhoneNumber] = useState('')
    const [password, setPassword] = useState('')
    const [fieldError, setFieldError] = useState('')
    const [showPassword, setShowPassword] = useState(false)
    const handleClickShowPassword = () => setShowPassword(!showPassword)
    const handleMouseDownPassword = () => setShowPassword(!showPassword)
    const [open, setOpen] = useState(false);
    const classes = useStyles();

    const onEnter = (event) => {
        if (event.keyCode === 13) {
            handleLogin()
        }
    }

    const validation = () => {
        let valid = true
        if(phoneNumber.length !== 10) {
            valid = false
            setFieldError("phoneError")
        }
        else if (password.length > 16 || password.length < 8) {
            valid = false
            setFieldError("passwordError")
        }
        return valid
    } 
    
      const handleLogin = () => {
        if(validation()) {
          setOpen(!open);
          props.onUpdateAuth({
            isLoggedIn: true
        })
        console.log(phoneNumber, password)
          props.history.push("/dashboard")
        }
      }

    return (
      
        <Container maxWidth="xl" className={classes.container}>
           <Backdrop className={classes.backdrop} open={open}>
             <CircularProgress />
          </Backdrop>
            <Card>
              <CardHeader color="textSecondary"         
                  title="Welcome"
              />
              <CardContent className={classes.card}>
                  <TextField 
                    type="email" 
                    required
                    error={fieldError==="phoneError"}
                    helperText={fieldError==="phoneError" ? "please enter valid phone number":""}
                    label="Phone number"
                    name="phoneNumber" 
                    variant="outlined"
                    InputProps={{inputProps: { pattern:/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/}}}
                    onChange={(event) => {setPhoneNumber(event.target.value); setFieldError('')}} 
                    className={classes.inputField}/>
                  <TextField 
                    type={ showPassword ? "text": "password"} 
                    required
                    error={fieldError==="passwordError"}
                    helperText={fieldError==="passwordError" ? "please enter valid password":""}
                    label="Password" 
                    variant="outlined"
                    className={classes.inputField}
                    InputProps={{ 
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={handleClickShowPassword}
                                  onMouseDown={handleMouseDownPassword}
                                >
                                  {showPassword ? <Visibility /> : <VisibilityOff />}
                                </IconButton>
                          </InputAdornment>
                      )
                    }}
                    onKeyDown={event => onEnter(event)}
                    onChange={(event) => {setPassword(event.target.value); setFieldError('')}} />
              </CardContent>
              <CardActions className={classes.actions}>
                  <Button className={classes.btnLogin} onClick={() => handleLogin()}>LOGIN</Button>
                <Link to="/register" style={{textDecoration: "none"}}>
                  <Button className={classes.btnRegister}>REGISTER</Button>
                </Link>
              </CardActions>
              <Link to="/" style={{textDecoration: "none"}}>
                <div className={classes.forgotPassword}>Forgot Password</div>
                </Link>
              {fieldError==="passwordError" ? <div style={{color:"red", textAlign:'left'}}>Password must have<ul><li>minimum 8 characters</li><li>maximum 16 characters</li></ul></div> :""}
            </Card>
        </Container>
    )
}

const mapStateToProps = state => ({
  authState: state.authState,
})

const mapDispatchToProps = {
  onUpdateAuth: updateAuthState
}


export default connect(mapStateToProps, mapDispatchToProps)(Login)