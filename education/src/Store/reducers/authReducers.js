import * as actionTypes from '../actions/actionTypes'

const initialState = { 
      isLoggedIn: false
    }
  

export default function authStateReducer(state=initialState, {type, payload}) {
    switch(type){
        case actionTypes.UPDATE_AUTH:
            return payload.authState;
        case actionTypes.REMOVE_AUTH:
            return state;
        default:
            return state;
    }
}