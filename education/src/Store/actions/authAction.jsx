import * as actionTypes from './actionTypes'

export function updateAuthState(authState){
    return{
        type: actionTypes.UPDATE_AUTH,
        payload: {
            authState: authState
        }
    }
}

export function removeAuth(authState){
    return{
        type: actionTypes.REMOVE_AUTH,
        payload: {
            authState: authState
        }
    }
}