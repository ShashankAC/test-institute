import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import TestCard from '../Components/TestCard/TestCard'

const useStyles = makeStyles((theme) => ({ 
    container: {
        display: 'flex',
        justifyContent: 'space-around'
    }
}))

function TestSeries() {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <TestCard title="Mains" color="#00C0EF"/>
            <TestCard title="Prelims" color="#00A65A"/>
        </div>
    )
}

export default TestSeries