import React,{ useState } from 'react'

import Card from '@material-ui/core/Card'
import Container from '@material-ui/core/Container'
import { makeStyles } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import TextField from '@material-ui/core/TextField';
import CardHeader from '@material-ui/core/CardHeader';
import Button from '@material-ui/core/Button';


const useStyles = makeStyles((theme) => ({
    btnRegister: {
        width: "90%",
        backgroundColor: "#FF1737",
        '&:hover': {
            background: "#FF1737",
         }
    },
    container: {
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#005794"
    },
    content: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        height: "100%;"
    },
    card: {
        width: "45%",
        justifyContent: "center",
        borderRadius: "15px",
    },
    cardHeader: {
        backgroundColor: "#dddddd"
    },
    form: {
        width: "100%",
        height: "60%",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    },
    textField: {
        width: "90%",
        margin: "5px"
    },
    cardActions: {
        display: 'flex',
        width: "100%",
        flexDirection: "column",
        alignItems: "center"
    },
    error: {
        height: "14px",
        color: "red"
    }
}))

function Register() {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [password, setPassword] = useState('')
    const [fieldError, setFieldError] = useState('')

    const classes = useStyles();

    const validation = () => {
        let valid = true
        const emailPattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        const namePattern = /^[a-zA-Z` .]$/
        if(phone.length !== 10) {
            valid = false
            setFieldError("Please enter valid mobile number")
        }
        else if(!emailPattern.test(email)) {
                valid = false
                setFieldError("Please enter valid Email ID")
            }
        else if (name.length === 0 || namePattern.test(name)){
            valid = false
            setFieldError("Please enter valid name")
        }
        else if (password.length > 16 || password.length < 8) {
            valid = false
            setFieldError("Password should have between 8 and 16 characters")
        }
        return valid
    }

    const submitHandler = event => {
        event.preventDefault();
        if(validation()) {
            // api call
            let data = {
                name: name,
                phone: phone,
                email: email,
                password: password
            }
            console.log(data)
        }
    }

    return (
        <Container maxWidth="xl" className={classes.container}>
            <Card className={classes.card}>
                <CardHeader 
                    title="REGISTRATION"
                    className={classes.cardHeader}/>
                <form onSubmit={submitHandler} className={classes.form}>
                    <CardContent className={classes.content}>
                        <TextField 
                            type="text"
                            required
                            className={classes.textField}
                            // label="Name"
                            placeholder="Name"
                            name="name"
                            value={name}
                            onChange={(event) => {setName(event.target.value); setFieldError("")}}
                        />
                        <TextField 
                            type="email"
                            required
                            className={classes.textField}
                            placeholder="Email ID"
                            name="email"
                            value={email}
                            onChange={(event) => {setEmail(event.target.value); setFieldError("")}}
                        />
                        <TextField 
                            type="tel"
                            required
                            className={classes.textField}
                            placeholder="Phone Number"
                            name="phone"
                            value={phone}
                            onChange={(event) => {setPhone(event.target.value); setFieldError("")}}
                        />
                        <TextField 
                            type="password"
                            required
                            className={classes.textField}
                            placeholder="Password"
                            name="password"
                            value={password}
                            onChange={(event) => {setPassword(event.target.value); setFieldError("")}}
                        />
                    </CardContent>
                    <CardActions className={classes.cardActions}>
                        <Button className={classes.btnRegister} type="submit">SUBMIT</Button>
                        <p className={classes.error}>{fieldError}</p>
                    </CardActions>
                </form>
            </Card>
        </Container>
    )
}

export default Register