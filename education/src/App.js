import React from 'react';
import { Route, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Login from '../src/Login/Login.jsx'
import Register from '../src/Register/Register'
import NavBar from '../src/NavBar/NavBar'
import Dashboard from '../src/Dashboard/Container/Dashboard'
import MyCourses from '../src/MyCourses/Container/MyCourses'
import TestSeries from '../src/TestSeries/Container/TestSeries'
import Profile from '../src/Profile/Container/Profile'
import { removeAuth } from "./Store/actions/authAction";
import classes from './App.module.css'


function App(props) {

  const AuthRoutes = () => {
    return (
      <div className={classes.App}>
        <Switch>
          <Route exact path="/" component={Login}/>
          <Route exact path="/register" component={Register} />
          <Route render={() => <Redirect to={{ pathname: "/" }} />} />
        </Switch>
      </div>
    )
  }

  const UserRoutes = () => {
    return (
      <div className={classes.App}>
          <NavBar />
          <main className={classes.container}>
            <Switch>
              <Route exact path="/dashboard" component={Dashboard} />
              <Route exact path="/mycourses" component={MyCourses} />
              <Route exact path="/testseries" component={TestSeries} />
              <Route exact path="/profile" component={Profile} />
              <Route render={() => <Redirect to={{ pathname: "/" }} />} />
            </Switch>
          </main>
      </div>
    );
  }
  
  return (
    <React.Fragment>
      {props.authState.isLoggedIn ?
      <Route component={UserRoutes} /> 
      : <Route component={AuthRoutes} />}
    </React.Fragment>
  );
}


const mapDispatchToProps = {
  onRemoveAuth: removeAuth,
}

const mapStateToProps = state => ({
  authState: state.authState,
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
