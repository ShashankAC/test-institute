import React from 'react'
import Container from '@material-ui/core/Container'
import Card from '@material-ui/core/Card'
import { makeStyles } from '@material-ui/core/styles';
import { CardHeader } from '@material-ui/core';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        width: "100%",
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },
    subjects: {
        margin: "5px",
        width: "30%",
        height: "200px",
        backgroundColor: "#7CCAD7"
    },
    cards: {
        width: "100%",
        display: 'flex',
        justifyContent:'center',
        flexWrap: 'wrap'
    },
    cardHeader: {
        backgroundColor: '#1976D2'
    }
}))

function Quiz() {

    const classes = useStyles();

    return (
        <Container className={classes.container}>
            <Typography color="primary" variant="h2">Quiz</Typography>
            <div className={classes.cards}>
                {["Physics", "Chemistry", "Maths", "Biology", "Computer Science", "Electronics"].map((sub, i) => (
                    <Card className={classes.subjects} key={i}>
                        <CardHeader 
                            title={<p style={{marginTop: "0px", fontSize: "15px"}}>{sub}</p>} 
                            className={classes.cardHeader}
                        />
                        <CardContent>

                        </CardContent>
                    </Card>
                ))}
            </div>
        </Container>
    )
}

export default Quiz