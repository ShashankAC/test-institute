import React from 'react'
// import Container from '../Container/MyCourses'
import PropTypes from 'prop-types';
import Course from '../Components/Course/Course'
import AccountBalanceOutlinedIcon from '@material-ui/icons/AccountBalanceOutlined';
import { makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme) => ({
    category: {
        display: 'flex',
        flexDirection: 'column',
        marginLeft:"5px"
    },
    courses: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around'
    },
    categoryName: {
        textAlign: "left",
        margin: "5px",
        fontWeight: "bold"
    },
    tabBar: {
      marginBottom: "5px",
      width:'100%'
    },
}))

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-auto-tabpanel-${index}`}
        aria-labelledby={`scrollable-auto-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  function a11yProps(index) {
    return {
      id: `scrollable-auto-tab-${index}`,
      'aria-controls': `scrollable-auto-tabpanel-${index}`,
      href: `#cat${index}`
    };
  }
  
function MyCourses() {
    const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

    return (
        <div>
            <AppBar position="static" color="default" className={classes.tabBar}>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    aria-label="scrollable auto tabs example"
                    >
                    <Tab label="Category One" {...a11yProps(0)} />
                    <Tab label="Category Two" {...a11yProps(1)} />
                    <Tab label="Category Three" {...a11yProps(2)} />
                </Tabs>
            </AppBar>
  
            {[{categoryName: "Category One", courses: [ {courseName: "course1", image: <AccountBalanceOutlinedIcon />},
                                                        {courseName: "course2", image: <AccountBalanceOutlinedIcon />},
                                                        {courseName: "course3", image: <AccountBalanceOutlinedIcon />}
                                                ]}, 
              {categoryName: "Category Two", courses: [ {courseName: "course4", image: <AccountBalanceOutlinedIcon />},
                                                        {courseName: "course5", image: <AccountBalanceOutlinedIcon />},
                                                        {courseName: "course6", image: <AccountBalanceOutlinedIcon />}
              ]}, 
              {categoryName: "Category Three", courses: [{courseName: "course7", image: <AccountBalanceOutlinedIcon />},
                                                         {courseName: "course8", image: <AccountBalanceOutlinedIcon />},
                                                         {courseName: "course9", image: <AccountBalanceOutlinedIcon />}
                                                ]}].map((category, index) => (
                                                    <section id={`cat${index}`} key={category.categoryName} className={classes.category}>
                                                        <p className={classes.categoryName}>{category.categoryName}</p>
                                                        <div className={classes.courses}>
                                                            {category.courses.map((course) => (
                                                                <Course key={course.courseName}
                                                                        image={course.image} 
                                                                        courseName={course.courseName}/>
                                                            ))}
                                                        </div>
                                                    </section>
                                                ))} 
        </div>
    )
  
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

export default MyCourses