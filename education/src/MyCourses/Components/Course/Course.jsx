import React from 'react'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent';
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    card: {
        width: "30%",
        height: "100px",
        minWidth:"250px",
        margin: "5px"
    },
    cardContent: {
        display: 'flex',
        justifyContent: "space-between"
    },
    courseData: {
        display: "flex",
        flexDirection: "column"
    }
}))



function Course(props) {

    const classes = useStyles();

    return (
        <Card className={classes.card}>
            <CardContent className={classes.cardContent}>
                <div className={classes.courseData}>
                    {props.image}
                    {props.courseName}
                </div>
                <div>
                    <AddBoxOutlinedIcon />
                </div>
            </CardContent>
        </Card>
    )
}

export default Course