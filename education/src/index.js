import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import storageSession from 'redux-persist/lib/storage/session'
import { persistStore, persistReducer } from "redux-persist";
import { combineReducers, createStore } from "redux";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { Router } from 'react-router-dom'
import history from "./Store/history";
import authStateReducer from "./Store/reducers/authReducers";


const allReducers = combineReducers({
  authState: authStateReducer 
})

const persistConfig = {
  key: 'root',
  // storage,
  storage: storageSession,
}

const persistedReducer = persistReducer(persistConfig, allReducers)

const store = createStore(persistedReducer)

const persistor = persistStore(store)

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Router history={history}>
        <App />
      </Router>
    </PersistGate>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
