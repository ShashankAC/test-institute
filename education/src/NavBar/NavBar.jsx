import React,{ useEffect } from 'react'
import clsx from 'clsx';
import { Link } from 'react-router-dom'
import { withRouter } from "react-router-dom";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Divider from '@material-ui/core/Divider';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Drawer from '@material-ui/core/Drawer';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { removeAuth } from '../Store/actions/authAction';
import { connect } from "react-redux";
import SpeedOutlinedIcon from '@material-ui/icons/SpeedOutlined';
import ImportantDevicesOutlinedIcon from '@material-ui/icons/ImportantDevicesOutlined';
import ClassOutlinedIcon from '@material-ui/icons/ClassOutlined';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import AccountBoxOutlinedIcon from '@material-ui/icons/AccountBoxOutlined';
import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';
import userImg from '../Assets/user.png'
import useMediaQuery from '@material-ui/core/useMediaQuery';
import SchoolOutlinedIcon from '@material-ui/icons/SchoolOutlined';
import useStyles from './NavBarCSS'

function NavBar(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(true);
    const [visibility, setVisibility] = React.useState("block")
    const matches = useMediaQuery('(min-width:700px)');

    const logout = () => {
        props.onRemoveAuth({ isLoggedIn:false })
    }

    useEffect(() => {
      if(matches) {
        setOpen(true)
        setVisibility("block")
      }
      else {
        setOpen(false)
        setVisibility("none")

      }
    },[matches])

    const handleDrawerOpen = () => {
        setOpen(true);
        setVisibility("block")
      };
    
      const handleDrawerClose = () => {
        setOpen(false);
        setVisibility("none")
      };
   
    return (
        <div>
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                [classes.appBarShift]: open,
                })}
            >
                <Toolbar className={classes.toolbar}>
                    <MenuIcon onClick={open ? handleDrawerClose: handleDrawerOpen}/>
                    <div className={classes.appPhoto}>
                      <img src={userImg} alt="img" className={classes.userImg}/>
                      <p>Student1</p>
                    </div>
                </Toolbar>

            </AppBar>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open,
                })}
                classes={{
                paper: clsx({
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                }),
                }}
            >
                <div className={classes.toolbar}>
                    <Typography variant="h6" className={classes.logo} >
                      UPSC LOUNGE 
                    </Typography>
                </div>
                <div className={classes.profile}>
                  <div>
                    <img src={userImg} alt="img" className={classes.userImg}/>
                  </div>
                  <div className={classes.studentInfo} style={{display: visibility}}>
                    <p>Student1</p>
                    <div className={classes.onlineStatus}>
                      <div className={classes.online}> </div>
                      <p>Online</p>
                    </div>
                  </div>
                </div>
                <List className={classes.list}>
                {[{text:'Dashboard', icon: <SpeedOutlinedIcon style={{color: 'white'}}/>, to: "/dashboard"}, 
                  {text: 'My Courses', icon: <SchoolOutlinedIcon style={{color: 'white'}}/>, to: "/mycourses"},
                  {text:'Important Updates', icon: <ImportantDevicesOutlinedIcon style={{color: 'white'}}/>, to: "/importantUpdates"},
                  {text:'Test Series', icon: <ClassOutlinedIcon style={{color: 'white'}}/>, to: "/testseries"}, 
                  {text:'Products', icon: <ShoppingCartOutlinedIcon style={{color: 'white'}}/>, to: "/products"},
                  {text:'Profile', icon: <AccountBoxOutlinedIcon style={{color: 'white'}}/>, to: "/profile"}].map((item, index) => (
                    <Link to={item.to} 
                          className={classes.link} 
                          key={index}
                          onClick={item.onclick ? () => logout(): () => { }}>
                      <ListItem button 
                                key={item.text} 
                                selected={props.history.location.pathname === item.to}>
                      <ListItemIcon>{item.icon}</ListItemIcon>
                      <ListItemText primary={item.text} />
                      </ListItem>
                    </Link>
                ))}
                    <ListItem 
                      button 
                      onClick={() => logout()} 
                      >
                      <ListItemIcon><ExitToAppOutlinedIcon style={{color: 'white'}}/></ListItemIcon>
                      <ListItemText primary={"Logout"} />
                    </ListItem>
                </List>
                <Divider />
            </Drawer>
        </div>
    )
}

const mapStateToProps = state => ({
    authState: state.authState,
});

const mapDispatchToProps = {
    onRemoveAuth: removeAuth,
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NavBar));