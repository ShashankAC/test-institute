import { makeStyles } from '@material-ui/core/styles';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        backgroundColor: '#3C8DBC',
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
      },
      hide: {
        display: 'none',
      },
      drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
        backgroundColor: '#222D32',
        height: '100%',
      },
      drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        }),
      },
      drawerClose: {
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
          width: theme.spacing(9) + 1,
        },
      },
      toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: "space-between",
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        backgroundColor: '#3C8DBC'
      },
      content: {
        flexGrow: 1,
        padding: theme.spacing(3),
      },
    logout: {
        color: 'white'
    },
    list: {
        backgroundColor: '#222D32',
        color: 'white',
        height: '100%'
    },
    logo: {
        color: 'white'
    },
    link: {
      textDecoration: 'none',
      color: "white"
    },
    userImg: {
      marginTop: "5px",
      height: "45px",
      width: "45px",
      borderRadius: "22.5px",
      marginRight: "2px" 
    },
    online: {
      backgroundColor: "green", 
      height: "8px", 
      width: "8px", 
      borderRadius: "4px",
      margin: "3px"
    },
    onlineStatus: {
      display: 'flex',
      justifyContent: 'space-around',
      alignItems: 'center'
    },
    profile: {
      display: 'flex',
      justifyContent: "space-around",
      alignItems: "center",
      backgroundColor: '#222D32',
      color: "white"
    },
    studentInfo: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent:'center',
      alignItems: 'center'
    },
    searchbar: {
      backgroundColor: "#222D32",
      color: "white",
    },
    searchField: {
      backgroundColor: "#374850",
      color: "white",
    },
    appPhoto: {
      display: "flex"
    }

}));

export default useStyles