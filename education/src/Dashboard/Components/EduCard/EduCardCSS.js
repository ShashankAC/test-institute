import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({ 
    card: {
       width: "100%",
       minWidth: "150px",
       color: "white",
       height: "150px",
       margin: "5px"
    },
    count: {
        color: "white",
        fontSize: "20px"
    },
    cardHeader: {
        marginBottom: "0px",
        paddingBottom: "0px"
    }
   }))

   export default useStyles