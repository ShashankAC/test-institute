import React, { useState, useEffect } from 'react'
import Card from '@material-ui/core/Card'
import useStyles from './EduCardCSS'
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
// import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined';
// import RefreshOutlinedIcon from '@material-ui/icons/RefreshOutlined';
// import SmsFailedOutlinedIcon from '@material-ui/icons/SmsFailedOutlined';
// import SentimentDissatisfiedOutlinedIcon from '@material-ui/icons/SentimentDissatisfiedOutlined';



function EduCard(props) {
    const classes = useStyles();
    

    // const [image, setImage] = useState()
    const [color, setColor] = useState()

    useEffect(() => {
      
        if (props.title === "Exams attended") {
            // setImage(<CheckBoxOutlinedIcon />)
            setColor("#00C0EF")
        }
        else if(props.title === "Past Exams") {
            // setImage(<RefreshOutlinedIcon />)
            setColor("#00A65A")
        }
        else if(props.title === "Tests Failed") {
            // setImage(<SentimentDissatisfiedOutlinedIcon />)
            setColor("#DD4B39")
        }
        else {
            // setImage(<SmsFailedOutlinedIcon />)
            setColor("#F39C12")
        }
    },[props])

    return (
        <Card  className={classes.card} style={{ backgroundColor: color }}>
            <CardHeader            
                title={<p>{props.title}</p>}
                className={classes.cardHeader}
            />
            <CardContent>
            <Typography variant="h6" component="h2">
                {props.count}
            </Typography>
            </CardContent>
        </Card>
    )
}

export default EduCard