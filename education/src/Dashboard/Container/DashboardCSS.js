import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
    cards: {
        display: "flex",
        justifyContent: "center",
        flexWrap: 'wrap',
        width: '100%'
    },
    verticalCards: {
        display: 'flex',
        justifyContent: "space-around",
        flexDirection: 'column',
        alignItems: 'center',
        flexWrap: 'wrap',
        width: '100%'
    },
    container: {
        textAlign: "left",
        width: '100%'
    },
    h2: {
        margin: "10px",
        fontSize: "25px"                
    },
    firstSet: {
        display: 'flex',
        width: '50%',
        justifyContent: 'space-around'
    },
    firstSetFlex: {
        display: 'flex',
        width: '100%',
        justifyContent: 'space-around'
    },
    secondSet: {
        display: 'flex',
        width: '100%',
        flexDirection: 'column',
        alignItems: 'start'
    },
    lineChart: {
        width: '100%'
    }
}))

export default useStyles