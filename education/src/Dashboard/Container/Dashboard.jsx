import React from 'react'
import Card from '../Components/EduCard/EduCard'
import useStyles from './DashboardCSS'
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const options = {
    chart: {
      type: 'line',
      reflow: true,
      events: {
        load: function(event) {
            event.target.reflow()
        }
    }
},
    title: {
      text: 'Your percentage graph in all papers'
    },
    series: [
        {
            name: 'Score',
            data: [60, 40, 80]
          }
    ],
    xAxis: {
        allowDecimals: false,
        title: {
            text: 'Tests'
        }
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: ''
        }
    },
    plotOptions: {
        series: {
          label: {
            connectorAllowed: true
          },
          pointStart: 1
        }
    }
};

function Dashboard() {
    const classes = useStyles();
    const matchesParent = useMediaQuery('(min-width:1115px)');
    const matchesChildren = useMediaQuery('(min-width:519px)');

    return (
        <div className={classes.container}>
            <p className={classes.h2}>Dashboard</p>
            <div className={matchesParent ? classes.cards: classes.verticalCards}>
                <div className={matchesParent ? classes.firstSet: matchesChildren ? classes.firstSetFlex: classes.secondSet}>
                    <Card title={"Exams attended"} count={"8"}/>
                    <Card title={"Past Exams"} count={"3"}/>
                </div>
                <div className={matchesParent ? classes.firstSet: matchesChildren ? classes.firstSetFlex:classes.secondSet}>
                    <Card title={"Tests Failed"} count={"3"}/>
                    <Card title={"XYZ"} count={"2"}/>
                </div>
            </div>
            <div className={classes.lineChart}> 
                <HighchartsReact    highcharts={Highcharts} 
                                    options={options}
                                    className={classes.highCharts} />
            </div>
        </div>
    )
}

export default Dashboard